document.addEventListener("DOMContentLoaded", function () {
    var layout = document.getElementById('layout'),
        pages = document.getElementsByClassName('page'),
        length = pages.length,
        position = 0,
        top = 0;

    //Добавление страницам свойства top для того.что бы их "спрятать" и оставить только первую//
    var showOne = function () {
        for (var i = 0; i < length; i++) {
            pages[i].style.top = top + '%';
            top += 100;
        }
    };
    //Приравниваем высоту контейнера к высоте страницы//
    var heightLayout = function () {
        layout.style.height = window.innerHeight + 'px';
    };

    window.onload = function () {
        heightLayout();
    };

    window.onresize = function () {
        heightLayout();
    };

    //Событие колесика мыши//
    var mouseEvent = function () {
        window.addEventListener("mousewheel", MouseWheelHandler, false);
        window.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
        function MouseWheelHandler(e) {
            var e = window.event || e;
            var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
            if (delta < 0) {
                var positionNow = length - 1;
                if (-positionNow * 100 == position)
                    return;

                slideDown();
            } else {
                if (position == 0)
                    return;

                slideUp();
            }
        }
    };
    //Пермещениие страниц с указаным парамтром//
    var transformPage = function (position) {
        layout.style.transform = 'translate3d(0px, ' + position + '%, 0px)';
        layout.style.MsTransform = 'translate3d(0px, ' + position + '%, 0px)';
        layout.style.WebkitTransform = 'translate3d(0px, ' + position + '%, 0px)';
        layout.style.Moztransform = 'translate3d(0px, ' + position + '%, 0px)';
        layout.style.Otransform = 'translate3d(0px, ' + position + '%, 0px)';
    };


    var slideDown = function () {
        position -= 100;
        transformPage(position);
    };

    var slideUp = function () {
        position += 100;
        transformPage(position);
    };
    //Событие нажатия клавиш "вниз" и "вверх"//
    document.onkeydown = function (e) {
        switch (e.which) {
            case 38:
                if (position != 0)
                slideUp();
                break;
            case 40:
                var positionNow = length - 1;
                if (-positionNow * 100 != position)
                    slideDown();
                break;
            default:
                return;
        }
    };

    mouseEvent();
    showOne();
});